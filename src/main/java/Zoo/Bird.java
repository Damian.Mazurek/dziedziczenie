package Zoo;

public class Bird extends Animal{

    private boolean hasWings;

    public boolean isHasWings() {
        return hasWings;
    }

    public void setHasWings(boolean hasWings) {
        this.hasWings = hasWings;
    }

    @Override
    public String toString() {
        return "Bird{" +
                "hasWings=" + hasWings +
                '}';
        
    }
}
