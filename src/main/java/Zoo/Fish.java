package Zoo;

public class Fish extends Animal{

    private boolean hasLungs;

    public boolean isHasLungs() {
        return hasLungs;
    }

    public void setHasLungs(boolean hasLungs) {
        this.hasLungs = hasLungs;
    }

    @Override
    public String toString() {
        return "Fish{" +
                "hasLungs=" + hasLungs +
                '}';
    }
}
