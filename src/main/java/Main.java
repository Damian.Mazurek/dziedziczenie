import Zoo.Animal;
import Zoo.Bird;
import Zoo.Fish;


public class Main {
    public static void main(String[] args) {

        Bird bird = new Bird();
        bird.setName("Andrzej");
        bird.setHasWings(true);
        bird.setColour("White");
        bird.setHasOwner(false);

        Fish fish = new Fish();
        fish.setName("Flądra");
        fish.setColour("Silver");
        fish.setHasLungs(true);
        fish.setHasOwner(true);

        System.out.println(bird.toString());

    }
}
